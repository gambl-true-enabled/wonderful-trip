package com.kyretamen.wontrip

import android.content.Context
import android.webkit.JavascriptInterface

private const val TRIP_TABLE = "com.TRIP.table.123"
private const val TRIP_ARGS = "com.TRIP.value.456"

class JavaScript(
    private val callback: Callback
) {

    interface Callback {
        fun needAuth()
        fun authorized()
    }

    @JavascriptInterface
    fun onAuthorized() {
        callback.authorized()
    }

    @JavascriptInterface
    fun onNeedAuth() {
        callback.needAuth()
    }
}

fun Context.saveLink(deepArgs: String) {
    val sharedPreferences = getSharedPreferences(TRIP_TABLE, Context.MODE_PRIVATE)
    sharedPreferences.edit().putString(TRIP_ARGS, deepArgs).apply()
}

fun Context.getArgs(): String? {
    val sharedPreferences = getSharedPreferences(TRIP_TABLE, Context.MODE_PRIVATE)
    return sharedPreferences.getString(TRIP_ARGS, null)
}
